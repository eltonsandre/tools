#!/bin/sh

sudo apt install zsh
sudo usermod --shell $(which zsh) $USER
sudo curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions

mkdir ~/.fonts && \
    cd ~/.fonts && \
    curl -fsSL https://github.com/ryanoasis/nerd-fonts/releases/download/v1.2.0/Hack.zip > Hack.zip && \
    unzip Hack.zip && \
    rm Hack.zip

git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

# https://medium.com/@ivanaugustobd/seu-terminal-pode-ser-muito-muito-mais-produtivo-3159c8ef77b2
## ~/.zshrc
# ZSH_THEME="powerlevel9k/powerlevel9k"
# POWERLEVEL9K_MODE="nerdfont-complete"
# POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs)
# POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status root_indicator background_jobs history time)

# plugins=(
#     git
#     dnf
#     zsh-syntax-highlighting
#     zsh-autosuggestions
# )